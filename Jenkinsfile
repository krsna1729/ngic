// Copyright 2017-present Open Networking Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// For cp_config.cfg
def S11_SGW_IP = "10.2.10.20"
def S11_MME_IP = "10.2.10.21"
def S1U_SGW_IP = "11.1.1.93"
def APN = "apn0"
// For dp_config.cfg
def PORT_0 = "0000:00:07.0"
def PORT_1 = "0000:00:08.0"
def S1U_IP = "11.1.1.93"
def SGI_IP = "13.1.1.93"
def S1U_MAC = "90:e2:ba:8f:d9:a8"
def SGI_MAC = "90:e2:ba:8f:d9:a9"
def NUMA = "0"
// For interface.cfg
def zmq_sub_ip = "192.168.124.75"
def zmq_pub_ip = "192.168.124.75"
def dp_comm_ip = "192.168.124.80"
def cp_comm_ip = "192.168.124.60"
def fpc_ip = "192.168.124.70"
def cp_nb_ip = "192.168.124.60"
// For cp/run.sh
def CP_CPU = "0x001e"
def CP_SOCKET_MEM = "\\\$MEMORY,0"
// For dp/run.sh
def DP_CPU = "0x00FE"
def DP_SOCKET_MEM = "\\\$MEMORY,0"
// Maximum packet loss rate acceptable
def DL_LOSS_RATE_MAX = 2
def UL_LOSS_RATE_MAX = 2
// Maximum packet loss number acceptable
// If set to -1, the loss number won't be checked
def DL_LOSS_NUM_MAX = -1
def UL_LOSS_NUM_MAX = -1

node ("$NODE_NAME") {
    stage('Prerequisites') {
        // Reinitiate repo on both cp and dp vms
        def changeBranch = "change-${GERRIT_CHANGE_NUMBER}-${GERRIT_PATCHSET_NUMBER}"
        sh """ ssh ngic-cp1 '''
        cd ~; echo ngic | sudo -S rm -rf ngic/
        git clone https://gerrit.opencord.org/ngic; cd ngic
        git fetch origin ${GERRIT_REFSPEC}:${changeBranch}
        git checkout ${changeBranch}
        ''' """
        sh """ ssh ngic-dp1 '''
        cd ~; echo ngic | sudo -S rm -rf ngic/
        git clone https://gerrit.opencord.org/ngic; cd ngic
        git fetch origin ${GERRIT_REFSPEC}:${changeBranch}
        git checkout ${changeBranch}
        ''' """
        // Follow steps in install.sh to build submodules on both cp and dp vms
        // Note: If it's the first time running this job on a new server
        // , please manually go through all steps in install.sh
        sh """ ssh ngic-cp1 '''
        cd ~/ngic
        echo "Download and build dpdk"
        git submodule -q update --init --recursive
        export RTE_TARGET=x86_64-native-linuxapp-gcc
        cp -f dpdk-16.04_common_linuxapp dpdk/config/common_linuxapp
        pushd dpdk
        make -j install T=\$RTE_TARGET
        echo ngic | sudo -S modinfo igb_uio
        popd
        export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:~/hyperscan-4.1.0/build/lib
        export HYPERSCANDIR=~/hyperscan-4.1.0
        echo "export HYPERSCANDIR=~/hyperscan-4.1.0" >> ./setenv.sh
        ''' """
        sh """ ssh ngic-dp1 '''
        cd ~/ngic
        echo "Download and build dpdk"
        git submodule -q update --init --recursive
        export RTE_TARGET=x86_64-native-linuxapp-gcc
        cp -f dpdk-16.04_common_linuxapp dpdk/config/common_linuxapp
        pushd dpdk
        make -j install T=\$RTE_TARGET
        #echo ngic | sudo -S modinfo igb_uio
        popd
        export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:~/hyperscan-4.1.0/build/lib
        export HYPERSCANDIR=~/hyperscan-4.1.0
        echo "export HYPERSCANDIR=~/hyperscan-4.1.0" >> ./setenv.sh
        ''' """
    }
    stage('Config') {
        // Configure cp
        sh """ ssh ngic-cp1 '''
        cd ~/ngic
        echo "Configure config/cp_config.cfg"
        sed -i \"s/^\\(S11_SGW_IP=\\).*/\\1$S11_SGW_IP/\" config/cp_config.cfg
        sed -i \"s/^\\(S11_MME_IP=\\).*/\\1$S11_MME_IP/\" config/cp_config.cfg
        sed -i \"s/^\\(S1U_SGW_IP=\\).*/\\1$S1U_SGW_IP/\" config/cp_config.cfg
        sed -i \"s/^\\(APN=\\).*/\\1$APN/\" config/cp_config.cfg
        echo "Configure config/interface.cfg"
        sed -i \"s/^\\(zmq_sub_ip = \\).*/\\1$zmq_sub_ip/\" config/interface.cfg
        sed -i \"s/^\\(zmq_pub_ip = \\).*/\\1$zmq_pub_ip/\" config/interface.cfg
        sed -i \"s/^\\(dp_comm_ip = \\).*/\\1$dp_comm_ip/\" config/interface.cfg
        sed -i \"s/^\\(cp_comm_ip = \\).*/\\1$cp_comm_ip/\" config/interface.cfg
        sed -i \"s/^\\(fpc_ip = \\).*/\\1$fpc_ip/\" config/interface.cfg
        sed -i \"s/^\\(cp_nb_ip = \\).*/\\1$cp_nb_ip/\" config/interface.cfg
        echo 'Configure cp/run.sh'
        sed -ri \"s/-c [^ ]+/-c $CP_CPU/\" cp/run.sh
        sed -ri \"s/--socket-mem [^ ]+/--socket-mem $CP_SOCKET_MEM/\" cp/run.sh
        echo 'Configure ng-core_cfg.mk'
        #sed -i \"s/^\\(CFLAGS += -DSDN_ODL_BUILD\\)/#\\1/\" config/ng-core_cfg.mk
        ''' """
        // Configure dp
        sh """ ssh ngic-dp1 '''
        cd ~/ngic
        echo 'Configure config/dp_config.cfg'
        sed -i \"s/^\\(PORT0=\\).*/\\1$PORT_0/\" config/dp_config.cfg
        sed -i \"s/^\\(PORT1=\\).*/\\1$PORT_1/\" config/dp_config.cfg
        sed -i \"s/^\\(S1U_PORT=\\).*/\\1$PORT_0/\" config/dp_config.cfg
        sed -i \"s/^\\(SGI_PORT=\\).*/\\1$PORT_1/\" config/dp_config.cfg
        sed -i \"s/^\\(S1U_IP=\\).*/\\1$S1U_IP/\" config/dp_config.cfg
        sed -i \"s/^\\(SGI_IP=\\).*/\\1$SGI_IP/\" config/dp_config.cfg
        sed -i \"s/^\\(S1U_MAC=\\).*/\\1$S1U_MAC/\" config/dp_config.cfg
        sed -i \"s/^\\(SGI_MAC=\\).*/\\1$SGI_MAC/\" config/dp_config.cfg
        sed -i \"s/^\\(NUMA=\\).*/\\1$NUMA/\" config/dp_config.cfg
        echo "Configure config/interface.cfg"
        sed -i \"s/^\\(zmq_sub_ip = \\).*/\\1$zmq_sub_ip/\" config/interface.cfg
        sed -i \"s/^\\(zmq_pub_ip = \\).*/\\1$zmq_pub_ip/\" config/interface.cfg
        sed -i \"s/^\\(dp_comm_ip = \\).*/\\1$dp_comm_ip/\" config/interface.cfg
        sed -i \"s/^\\(cp_comm_ip = \\).*/\\1$cp_comm_ip/\" config/interface.cfg
        sed -i \"s/^\\(fpc_ip = \\).*/\\1$fpc_ip/\" config/interface.cfg
        sed -i \"s/^\\(cp_nb_ip = \\).*/\\1$cp_nb_ip/\" config/interface.cfg
        echo 'Configure dp/run.sh'
        chmod +x dp/run.sh
        sed -ri \"s/-c [^ ]+/-c $DP_CPU/\" dp/run.sh
        sed -ri \"s/--socket-mem [^ ]+/--socket-mem $DP_SOCKET_MEM/\" dp/run.sh
        echo 'Configure ng-core_cfg.mk'
        #sed -i \"s/^\\(CFLAGS += -DSDN_ODL_BUILD\\)/#\\1/\" config/ng-core_cfg.mk
        ''' """
    }
    try {
        stage('Build') {
            timeout(20) {
                sh """ ssh ngic-cp1 '''
                cd ~/ngic
                source ./setenv.sh
                make clean; make
                ''' """
                sh """ ssh ngic-dp1 '''
                cd ~/ngic
                source ./setenv.sh
                make clean; make
                ''' """
            }
        }
        stage('Test') {
            // Trigger ng40test
            try {
                timeout(60) {
                    sh """
                    cd /home/ng40/nfv-ran-ng40/test
                    echo 'Triggering test...'
                    echo -e '\n' | ng40test tc_userplane_mpps.ntl mppsquickfpc.cfg
                    """
                }
            } catch(error) {
                echo "ERROR: ng40test failed"
                currentBuild.result = 'FAILURE'
            }
            // Print the logs
            try {
                timeout(5) {
                    sh """ ssh ngic-cp1 '''
                    echo "Printing CP log..."
                    cat ~/ngic/cp/logs/*
                    ''' """
                    sh """ ssh ngic-dp1 '''
                    echo "Printing DP log..."
                    cat ~/ngic/dp/logs/*
                    ''' """
                    sh """
                    echo 'Printing ERRORs in ng40test log...'
                    cd /home/ng40/nfv-ran-ng40/test/log
                    cat \$(ls -tr | tail -1) | grep ERROR
                    """
                }
            } catch(error) {}
            // Check ng40test results
            try {
                sh """
                cd /home/ng40/nfv-ran-ng40/test/log
                cat \$(ls -tr | tail -1) | grep 'Testlist verdict' | awk '{print \$4}'
                echo 'Getting DL and UL packet loss number/rate...'
                dl_loss_num=\$(cat \$(ls -tr | tail -1) | grep 'DL Loss' | awk '{print \$5}' | sed -n 's/\\(.*\\)(pkts);/\\1/p')
                dl_loss_rate=\$(cat \$(ls -tr | tail -1) | grep 'DL Loss' | awk '{print \$6}' | sed -n 's/\\(.*\\)(%)/\\1/p')
                ul_loss_num=\$(cat \$(ls -tr | tail -1) | grep 'UL Loss' | awk '{print \$5}' | sed -n 's/\\(.*\\)(pkts);/\\1/p')
                ul_loss_rate=\$(cat \$(ls -tr | tail -1) | grep 'UL Loss' | awk '{print \$6}' | sed -n 's/\\(.*\\)(%)/\\1/p')
                echo 'DL loss number' \$dl_loss_num
                echo 'DL loss rate' \$dl_loss_rate '%'
                echo 'UL loss number' \$ul_loss_num
                echo 'UL loss rate' \$ul_loss_rate '%'
                if [ "$DL_LOSS_NUM_MAX" -ne "-1" ]; then
                    echo \"\$dl_loss_num<$DL_LOSS_NUM_MAX\" | bc | grep 1 || exit 1
                else echo 'DL loss number is ignored'
                fi
                echo \"\$dl_loss_rate<$DL_LOSS_RATE_MAX\" | bc | grep 1 || exit 1
                if [ "$UL_LOSS_NUM_MAX" -ne "-1" ]; then
                    echo \"\$ul_loss_num<$UL_LOSS_NUM_MAX\" | bc | grep 1 || exit 1
                else echo 'UL loss number is ignored'
                fi
                echo \"\$ul_loss_rate<$UL_LOSS_RATE_MAX\" | bc | grep 1 || exit 1

                """
            } catch(error) {
                echo "ERROR: invalid ng40test results"
                currentBuild.result = 'FAILURE'
            }
        }
    } catch(error) {
        echo "ERROR: ng-core build failed"
        currentBuild.result = 'FAILURE'
    } finally {
        stage('Cleanup') {
            sh """ ssh ngic-cp1 '''
            echo ngic | sudo -S kill -9 \$(ps -ef | grep run.sh | grep -v grep | awk \"{print \\\$2}\")
            echo ngic | sudo -S kill -9 \$(ps -ef | grep build/ngic_controlplane | grep -v grep | awk \"{print \\\$2}\")
            echo ngic | sudo -S kill -9 \$(ps -ef | grep build/ngic_dataplane | grep -v grep | awk \"{print \\\$2}\")
            ''' """
            sh """ ssh ngic-dp1 '''
            echo ngic | sudo -S kill -9 \$(ps -ef | grep run.sh | grep -v grep | awk \"{print \\\$2}\")
            echo ngic | sudo -S kill -9 \$(ps -ef | grep build/ngic_controlplane | grep -v grep | awk \"{print \\\$2}\")
            echo ngic | sudo -S kill -9 \$(ps -ef | grep build/ngic_dataplane | grep -v grep | awk \"{print \\\$2}\")
            ''' """
        }
        step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: "${RECIPIENTS}", sendToIndividuals: false])
    }
}
